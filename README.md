# TaskRunner
**a craft2 plugin by [24hoursmedia](http://www.24hoursmedia.com)**



<img src="taskrunner/doc/resources/taskrunner-logo.png" style="float: left; margin-right: 5px; margin-bottom: 5px;" align="left"  />

Craft CMS comes with a service that runs tasks asynchronous in the background.  

By default these tasks are executed after an http request of the CP control panel.

This plugin allows to run these tasks by a cron job *without any http requests at all*, not even internally.


**Why this plugin?**

During development of a tool to migrate content, we noticed a lot of tasks being generated
(of type UpdateElementSlugsAndUri).

The amount of tasks led to a lockdown of the CP and a halt of task processing. We had to clear the
tasks manually in the database to make the system work agan.

Additionally, http requests are not ideal for background task processing.
Timeouts, memory limits and restrictions on max. fpm-processes may intervene with the processing.

Processing tasks through cron jobs allows for greater scalability and stability.

**status**

This plugin has been tested against the migration job mentioned earlier.


## installation

Store the plugin directory in the craft/plugins folder

## configuration

### disable default task runner

You must disable the default task runner invocation by setting the configuration
option runTasksAutomatically to false:

in craft/config/general.php
```
// (...)
return array(

    // (...)
    
    'runTasksAutomatically' => false,
    
    // (...)
    
);
```

### set up a cron job

assuming your Craft 2 CMS is installed in */opt/craft-app/current*,

add the following line to your cronjobs using *crontab -e*:

    */1 * * * * php /opt/craft-app/current/craft/app/etc/console/yiic taskrunner execute

This will (by default configuration) attempt to process tasks 
for 55 seconds, with a pause of 0.2 seconds between each check for pending tasks.

If you have long running tasks or want to increase processing rate, you can do so as mentioned in 'advanced'

## run manually

For testing (or perhaps some other purpose) you can run the command manually.

In your craft installation folder:

    php craft/app/etc/console/yiic taskrunner execute 

## advanced

### taskrunner options

You can pass options to the *taskrunner execute* command to set the maximum runtime and pause between processing of tasks.

Available options:

*--runtime=*N**
   
the command will run for this time + the time needed for the last task to execute.
So, assuming that no task will run for longer than 5 seconds, you can set this to 55 (default)
if your cronjob runs every minute.

*--pausetime=*N**

The number of seconds to wait between checking for pending tasks. This value prevents high system loads
caused by querying the database for tasks; especially if you have many tasks.
You can enter a float, the default is 0.2 seconds.

**Example**

If you want a cronjob that checks tasks for 10 minutes, and a speedy processing of tasks
(and server load does not matter because you might have a dedicated worker machine), you can
set up the cron like this:

    php craft/app/etc/console/yiic taskrunner execute --runtime=590 --pausetime=0.05

### using supervisor.d

Some services exist that ensure a process (in this case the taskrunner) is always running.

This might be a better option than using cron jobs, as this ensures only one task runner is active,
even in the case when some tasks run very long.

Consider for example a task that runs for 2 minutes, and your cronjob activates the runner every minute.

We often use supervisor.d to ensure a service is up and running.
Contact us if you want advise on supervisor.d configuration.