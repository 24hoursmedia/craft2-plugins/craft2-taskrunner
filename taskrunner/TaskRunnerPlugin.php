<?php

namespace Craft;

/**
 * Date: 17/06/2017
 */
class TaskRunnerPlugin extends BasePlugin
{
    function getName()
    {
        return Craft::t('24HOURSMEDIA TaskRunner');
    }

    function getVersion()
    {
        return '0.1';
    }

    function getDeveloper()
    {
        return '24hoursmedia';
    }

    function getDeveloperUrl()
    {
        return 'http://www.24hoursmedia.com';
    }
}