<?php

namespace Craft;

/**
 * Date: 17/06/2017
 */
class TaskRunnerCommand extends BaseCommand
{

    private $defaultRunTime = 55;
    private $defaultPauseTime = 0.2;

    /**
     * Writes a line to stdout
     *
     * @param string $text
     *
     * @return $this
     */
    private function writeln($text = '')
    {
        echo $text . PHP_EOL;

        return $this;
    }

    public function actionIndex()
    {

        $this
            ->writeln('Craft2 TaskRunner')
            ->writeln()
            ->writeln('Execute yiic `taskrunner execute` to run tasks.')
            ->writeln('Or, yiic `taskrunner execute --runtime=30 --pausetime=0.5` to run tasks for 30 seconds with a pause of 0.5 seconds.')
            ->writeln()
        ;
    }

    /**
     * Executes an action processing cycle
     *
     * @param null $runtime
     * @param null $pausetime
     * @param int $maxtasks
     *
     * @return int
     */
    public function actionExecute($runtime = null, $pausetime = null, $maxtasks = PHP_INT_MAX)
    {
        if (boolval(craft()->config->get('runTasksAutomatically'))) {
            $this->writeln('You must set the `runTasksAutomatically` configuration option to `false` in your configuration');
            return 1;
        }


        $maxRuntime = $runtime ? (int)$runtime : $this->defaultRunTime;
        $pausetime = $pausetime ? (float)$pausetime : $this->defaultPauseTime;
        $this->writeln("Running tasks for $runtime seconds with pause $pausetime seconds between tasks");

        $started = microtime(true);
        $actionsExecuted = 0;
        $cycle = 0;
        $runningTime = 0;
        $numErrors = 0;

        $linePrefix = function () use (&$runningTime) {
            $t = str_pad(sprintf('%0.1f', $runningTime), 5, 0, STR_PAD_LEFT);

            return $t . ' secs: ';
        };

        while ($runningTime < $maxRuntime && $actionsExecuted < $maxtasks) {
            $runningTime = microtime(true) - $started;
            $this->writeln($linePrefix() . 'secs: ' . "cycle $cycle - $actionsExecuted actions executed until now with $numErrors errors");

            try {
                if ($this->attemptTaskExecution()) {
                    $actionsExecuted++;
                }
            } catch (\Exception $e) {
                Craft::log('ERROR: ' . $e->getMessage(), LogLevel::Error);
                $numErrors++;
            }

            $this->writeln($linePrefix() . "sleeping $pausetime secs");
            usleep($pausetime * 1E6);
            $cycle++;
        }
        $this->writeln($linePrefix() . 'secs: ' . "DONE at cycle $cycle - $actionsExecuted actions executed until now with $numErrors errors");
        $this->writeln();

        return 0;
    }

    /**
     * Attempts to execute a task. Return true if a task was found and executed
     *
     * @return boolean
     */
    protected function attemptTaskExecution()
    {
        $taskService =craft()->tasks;
        $task = $taskService->getNextPendingTask();
        if ($task) {
            // set the private property _runningTask
            $refObject   = new \ReflectionObject( $taskService );
            $hasRunningTaskProperty = $refObject->hasProperty('_runningTask');
            $refProperty = null;
            if ($hasRunningTaskProperty) {
                $refProperty = $refObject->getProperty('_runningTask');
                $refProperty->setAccessible(true);
                $refProperty->setValue($taskService, $task);
                $refProperty->setAccessible(false);
            }

            $taskService->runTask($task);

            // set the private property _runningTask
            if ($hasRunningTaskProperty) {
                $refProperty->setAccessible(true);
                $refProperty->setValue($taskService, null);
                $refProperty->setAccessible(false);
            }

            return true;
        }
        return false;
    }

}